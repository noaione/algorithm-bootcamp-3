
#include <stdio.h>
#include <string.h>

// Cross-platform :D
#ifdef _WIN32
#include <windows.h>
#include <conio.h>
#else
#ifdef __MINGW32__
#include <ncurses/curses.h>
#else
#include <curses.h>
#endif
#include "extras/unix_support.h"
#endif

#define keyUp 72 // arrow keys
#define keyDown 80
#define keyLeft 75
#define keyRight 77
#define keyEnter 13

void screenTitle(int *num) {
    /* VARIABLES */
    int titleScreen[10][69] = {{1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0},
                               {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0},
                               {1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0},
                               {1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0},
                               {1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0},
                               {1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0},
                               {1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0},
                               {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0},
                               {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0}};

    COORD cursorPosition;
    cursorPosition.X = 0;
    cursorPosition.Y = 0;
#ifdef _WIN32
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursorPosition);
#else
    SetCursorPos(cursorPosition);
#endif

    puts("");
    for (int i = 0; i < 10; i++)
    {
        printf("     ");
        for (int j = 0; j < 69; j++)
        {
            if (titleScreen[i][j])
            {
                int state = (j + *num + 3 * i) % 69;
                if ((0 <= state && state <= 9) || (60 <= state && state <= 69))
                {
                    printf("%c", char(176));
                }
                else if ((10 <= state && state <= 19) || (50 <= state && state <= 59))
                {
                    printf("%c", char(177));
                }

                else if ((20 <= state && state <= 29) || (40 <= state && state <= 49))
                {
                    printf("%c", char(178));
                }
                else
                {
                    printf("%c", char(219));
                }
            }
            else
                printf(" ");
        }
        puts("");
    }
}

int main() {
    char key;
    int x = 1;
    int num;
    int currPos = 0;
    char options[3][15] = {"MENU1", "MENU2", "MENU3"};
    do
    {
        screenTitle(&num);
        num += 68;
        printf("                       ");
        for (int i = 0; i < 30; i++)
        {
            if (i == 0)
                printf("%c", char(201));
            else if (i == 29)
                printf("%c", char(187));
            else
                printf("%c", char(205));
        }
        puts("");
        printf("                       %c                            %c\n", char(186), char(186));
        for (int i = 0; i < 3; i++)
        {
            printf("                       %c   ", char(186));
            if (currPos == i)
                printf(">>   ");
            else
                printf("     ");

            printf("%s", options[i]);
            for (int j = strlen(options[i]); j < 12; j++)
                printf(" ");

            if (currPos == i)
                printf("   <<");
            else
                printf("     ");
            printf("   %c", char(186));
            puts("");
        }
        printf("                       %c                            %c\n", char(186), char(186));
        printf("                       ");
        for (int i = 0; i < 30; i++)
        {
            if (i == 0)
                printf("%c", char(200));
            else if (i == 29)
                printf("%c", char(188));
            else
                printf("%c", char(205));
        }
        if (kbhit())
        {
            key = getch();
            if ((key == keyUp || key == keyLeft) && currPos != 0)
                currPos--;
            else if ((key == keyDown || key == keyRight) && currPos != 6)
                currPos++;
            else if (key == keyEnter)
            {
            }
        }

    } while (x != 0);

    return 0;
}
