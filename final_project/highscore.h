typedef struct highscore {
	char name[20];
	int score;
};

void swap(highscore *a, highscore *b);
void listName();
void listScore();
void processScore(highscore a);
void inputUsername(int score);
void printLeaderboard();