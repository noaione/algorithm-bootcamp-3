#include <stdio.h>

int main() {
    int num1, num2, num3;
    printf("Enter three number (x, y, z): ");
    scanf("%d %d %d", &num1, &num2, &num3);
    int num_arr[3] = {num1, num2, num3};

    // Main func, not scalable very well.
    int lowest = num_arr[0];
    for (int i = 0; i < 3; i++) {
        if (num_arr[i] < lowest) {
            lowest = num_arr[i];
        };
    };

    printf("Lowest number: %d", lowest);
    return 0;
}