#include <stdio.h>

void swap(int *a, int *b)
{
   int temp;
   temp = *b;
   *b = *a;
   *a = temp;
}

int main() {
    int x = 5, y = 3;
    printf("Before: %d %d\n", x, y);
    swap(&x, &y);
    printf("After: %d %d", x, y);
}