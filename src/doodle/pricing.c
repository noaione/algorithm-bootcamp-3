#include <stdio.h>

float percentage(float number, int percent) {
    return number * (percent / 100);
}

int main() {
    long int input_price;
    float final_price;
    int discount, tax;
    printf("Masukan harga barang: ");
    scanf("%ld", &input_price);

    printf("Masukan diskon (Dalam persen): ");
    scanf("%d", &discount);

    printf("Masukan pajak (Dalam persen): ");
    scanf("%d", &tax);

    // Calc discount.
    final_price = input_price - percentage(input_price, discount);
    printf("%f\n", percentage(input_price, discount));
    // Add tax after discount.
    final_price = final_price + percentage(final_price, tax);

    printf("Harga setelah diskon dan pajak: Rp%.2f", final_price);
}