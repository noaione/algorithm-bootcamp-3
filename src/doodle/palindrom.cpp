#include <stdio.h>
#include <string.h>

int palindrome(char str[], int i, int j_max) {
    if (str[i] == '\0') {
        return 1;
    }
    if (str[i] != str[j_max - 1]) {
        return 0;
    }
    palindrome(str, i+1, j_max-1);
}

int main() {
    char doc[100];
    scanf("%s", &doc);
    int palindrom = palindrome(doc, 0, strlen(doc));
    palindrom == 1 ? printf("Palindrome") : printf("Nope");
}