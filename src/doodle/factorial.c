#include <stdio.h>
#include <stdint.h>

long long int factorial(long long int n) {
    if (n < 0) {
        return n;
    }
    if (n == 0) {
        return 1;
    } else {
        return n * factorial(n - 1);
    };
};

int main() {
    long long int in;
    printf("Masukan input: ");
    scanf("%lld", &in);

    long long int result = factorial(in);
    printf("Hasil faktorial: %lld", result);
    return 0;
}
