#include <stdio.h>

// Initialize with 0 for simpler function calling.
void print_per_char(char str[], int i = 0) {
    if (str[i] == '\0') {
        return;
    }
    printf("%c", str[i]);
    print_per_char(str, i + 1);
}

int main() {
    char doc[100];
    scanf("%s", &doc);
    print_per_char("Recursive: ");
    print_per_char(doc);
    
}