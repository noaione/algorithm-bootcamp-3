#include <stdio.h>

int main() {
    int num;
    scanf("%d", &num);
    printf("%d\n", num % 10);
    if (num % 2 == 0) {
        printf("num is even.");
    } else {
        printf("num is odd.");
    }
}