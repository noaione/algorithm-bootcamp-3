#include <stdio.h>

int main() {
    int n, m, t;
    int mtrxA[105][105];
    int mtrxB[105][105];
    scanf("%d %d", &n, &m);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            scanf("%d", &mtrxA[i][j]);
        }
    }
    printf("\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            scanf("%d", &mtrxB[i][j]);
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("%d ", mtrxA[i][j] + mtrxB[i][j]);
        }
        printf("\n");
    }
}