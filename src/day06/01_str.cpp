#include <stdio.h>
#include <string.h>

int main() {
    int caps = 0, no_caps = 0, full_caps = 0;
    char chars[105];
    scanf("%s", &chars);
    int len = strlen(chars);
    for (int i = 0; i < len; i++) {
        if (chars[i] >= 'A' && chars[i] <= 'Z') {
            caps++;
        } else if (chars[i] >= 'a' && chars[i] <= 'z') {
            no_caps++;
        }
    }

    if (caps > no_caps) {
        full_caps = 1;
    }

    for (int i = 0; i < len; i++) {
        if (full_caps) {
            if (chars[i] >= 'a' && chars[i] <= 'z') {
                chars[i] -= 32;
            }
        } else {
            if (chars[i] >= 'A' && chars[i] <= 'Z') {
                chars[i] += 32;
            }
        }
    }

    printf("%s", chars);
}