#include <stdio.h>

int main() {
    int n, pa = 0, pb = 0, r;
    int mtrxA[105][105];
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            scanf("%d", &mtrxA[i][j]);
        }
    }
    for (int i = 0; i < n; i++) {
        pa += mtrxA[i][i];
    }
    int ij = 0;
    while (n > 0) {
        pb += mtrxA[ij][n-1];
        n--;
        ij++;
    }
    r = pa - pb;
    if (r < 0) {
        r *= -1;
    }
    printf("%d", r);
}