#include <stdio.h>

int glc = 0;

void bubbleSort(int arr[], int n) {
    bool swapped;
    do {
        swapped = false;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] > arr[i+1]) {
                int temp = arr[i+1];
                arr[i+1] = arr[i];
                arr[i] = temp;
                swapped = true;
                glc++;
            }
        }
        n--;
    } while (swapped);
}

int main() {
    int n;
    scanf("%d", &n);
    int arr[n] = {0};
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    bubbleSort(arr, n);
    printf("%d", glc);
}