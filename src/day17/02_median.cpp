#include <stdio.h>

void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        int cursor = arr[i], j = i - 1;
        while (j >= 0 && cursor < arr[j]) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = cursor;
    }
}

int main() {
    int n;
    scanf("%d", &n);
    int arr[n] = {0};
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    insertionSort(arr, n);
    printf("%d", arr[n / 2]);
}