#include <stdio.h>
#define ll long long

void selectionSort(ll arr[], int n) {
    for (int i = 0; i < n - 1; i++) {
        int minDex = i;
        for (int j = i + 1; j < n; j++) {
            if (arr[j] < arr[minDex]) {
                minDex = j;
            }
        }
        int temp = arr[i];
        arr[i] = arr[minDex];
        arr[minDex] = temp;
    }
}


int main() {
    ll arrEv[1000], arrOdd[1000];
    int n, evC = 0, odC = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        ll t;
        scanf("%lld", &t);
        if (t % 2 == 0) {
            arrEv[evC] = t;
            evC++;
        } else {
            arrOdd[odC] = t;
            odC++;
        }
    }
    selectionSort(arrEv, evC);
    selectionSort(arrOdd, odC);
    for (int i = 0; i < odC; i++) {
        printf("%lld ", arrOdd[i]);
    }
    for (int i = evC - 1; i >= 0; i--) {
        printf("%lld ", arrEv[i]);
    }
}