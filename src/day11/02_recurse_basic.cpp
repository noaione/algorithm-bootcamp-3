#include <stdio.h>

long long int recurse(long long int m, long long int n) {
    if (m == 0 || m == n) {
        return 1;
    }
    if (m < n) {
        return recurse(m - 1, n - 1) + recurse(m, n - 1);   
    }
}

int main() {
    long long int m, n;
    scanf("%lld %lld", &n, &m);
    printf("%lld", recurse(m, n));
}