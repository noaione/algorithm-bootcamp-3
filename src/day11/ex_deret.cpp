#include <stdio.h>

long long int deret(long long int n) {
    if (n <= 1) {
        return n;
    }
    return n + deret(n - 1);
};

int main() {
    long long int n;
    scanf("%lld", &n);
    printf("%lld", deret(n));
}