#include <stdio.h>

int main() {
    int H, A, n;
    scanf("%d %d", &H, &A);
    n = H % A == 0 ? (H / A) : (H / A) + 1;
    printf("%d", n);
    return 0;
}