#include <stdio.h>

int main() {
    int t, word_list[26];
    char word[105];
    scanf("%d", &t);
    scanf("%s", &word);
    for (int i = 0; i < t; i++) {
        if (word[i] >= 'A' && word[i] <= 'Z') {
            word_list[word[i] - 'A'] = 1;
        } else {
            word_list[word[i] - 'a'] = 1;
        }
    }
    for (int i = 0; i < 26; i++) {
        if (word_list[i] != 1) {
            printf("NO");
            return 0;
        }
    }
    printf("YES");
}