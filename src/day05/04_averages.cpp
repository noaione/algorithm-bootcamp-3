#include <stdio.h>
int main() {
    int c;
    scanf("%d", &c);
    while (c > 0) {
        int nc, dataset[1005], count = 0;
        double avg, sum = 0.0;
        scanf("%d", &nc);
        for (int i = 0; i < nc; i++) {
            scanf("%d", &dataset[i]);
            sum += dataset[i];
        }
        avg = sum / (double)nc;
        for (int j = 0; j < nc; j++) {
            if (dataset[j] > avg) {
                count++;
            }
        }
        printf("%.3f%%\n", (double)count / (double)nc * 100.0);
        c--;
    }
}