#include <stdio.h>
int main() {
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        int sp = (n-i) - 1;
        int hash = i + 1;
        for (int ij = 0; ij < sp; ij++) {
            printf(" ");
        }
        for (int ji = 0; ji < hash; ji++) {
            printf("#");
        }
        printf("\n");
    }
}