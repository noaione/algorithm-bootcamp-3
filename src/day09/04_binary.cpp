#include <stdio.h>
#include <string.h>

// Ported shamelessly from
// https://codeforces.com/contest/92/submission/22662471
int main() {
    int y = 0, z = 0, ops;
    char n[1000];
    scanf("%s", &n);
    int x = strlen(n);
    for (int i = x - 1; i >= 0; i--) {
        if (n[i] == 49) {
            if (y == 0) {
                y = i;
            }
            z++;
        }
    }
    int ex = z > 1 ? 2 : 0;
    printf("%d", x + y - z + ex);
}