#include <stdio.h>

int main() {
    // A!a#4ABB2z%bz8BzBzbZ
    int ascii[127] = {0};
    int ntotal = 0, nsym = 0, nabcxyz = 0;
    char word[105];
    scanf("%s", &word);
    for (int i = 0; word[i] != '\0'; i++) {
        if (word[i] >= '0' && word[i] <= '9') {
            ntotal++;
        } else if (word[i] >= '!' && word[i] <= '/') {
            nsym++;
        }
        ascii[word[i]]++;
    }
    for (int i = 0; i < 127; i++) {
        if (ascii[i] > 0) {
            if (i >= 'A' && i <= 'Z') {
                printf("%c: %d\n", i, ascii[i]);
                nabcxyz += ascii[i];
            } else if (i >= 'a' && i <= 'z') {
                printf("%c: %d\n", i, ascii[i]);
                nabcxyz += ascii[i];
            }
        }
    }
    printf("Total Numbers: %d\n", ntotal);
    printf("Sum: %d\n", nabcxyz);
    printf("Total Symbol: %d\n", nsym);
    printf("Symbol: ");
    for (int i = 47; i != 32; i--) {
        if (ascii[i] > 0) {
            printf("%c", i);
        }
    }
}