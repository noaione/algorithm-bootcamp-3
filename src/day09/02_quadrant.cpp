#include <stdio.h>

int main() {
    int a, b, pos;
    scanf("%d", &a);
    scanf("%d", &b);
    if (a > 0 && b > 0) {
        pos = 1;
    } else if (a < 0 && b > 0) {
        pos = 2;
    } else if (a < 0 && b < 0) {
        pos = 3;
    } else if (a > 0 && b < 0) {
        pos = 4;
    }
    printf("%d", pos);
}