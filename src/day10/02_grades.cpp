#include <stdio.h>

int main() {
    char strs[1005];
    int f = 0, t = 0;
    scanf("%s", &strs);
    for (int i = 0; strs[i] != '\0'; i++) {
        if (strs[i] == '2') {
            t++;
        } else if (strs[i] == '5') {
            f++;
        }
    }
    if (f == t) {
        printf("=");
    } else if (f > t) {
        printf("5");
    } else if (t > f) {
        printf("2");
    }
}