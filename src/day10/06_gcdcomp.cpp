#include <stdio.h>

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int ev[2*n+5], od[2*n+5], ctE = 0, ctO = 0, pain = 0;
        for (int i = 0; i < (2 * n); i++) {
            int temp;
            scanf("%d", &temp);
            if (temp % 2 == 0) {
                ev[ctE++] = i + 1;
                pain++;
            } else {
                od[ctO++] = i + 1;
            }
        }

        if (pain % 2 != 0) {
            ctE--;
            ctO--;
        } else {
            if (ctE >= ctO) {
                ctE -= 2;
            } else {
                ctO -= 2;
            }
        }
		for (int i = 0; i < ctE; i += 2) {
			printf("%d %d\n", ev[i], ev[i + 1]);
		}
		for (int i = 0; i < ctO; i += 2) {
			printf("%d %d\n", od[i], od[i + 1]);
		}
    }
}