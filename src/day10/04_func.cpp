#include <stdio.h>

int max_of_four(int a, int b, int c, int d) {
    int ln[4] = {a, b, c, d};
    int max = ln[0];
    for (int i = 0; i < 4; i++) {
        if (ln[i] > max) {
            max = ln[i];
        }
    }
    return max;
}

int main() {
    int a, b, c, d;
    scanf("%d", &a);
    scanf("%d", &b);
    scanf("%d", &c);
    scanf("%d", &d);
    int res = max_of_four(a, b, c, d);
    printf("%d", res);
}