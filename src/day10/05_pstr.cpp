#include <stdio.h>

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; i++) {
        char strs[1005];
        int res = 0;
        scanf("%s", &strs);
        for (int j = 0; strs[j] != '\0'; j++) {
            if (strs[j] >= '0' && strs[j] <= '9') {
                res += strs[j] - 48;
            }
        }
        printf("%d\n", res);
    }
}