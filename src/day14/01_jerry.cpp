#include <stdio.h>

int main() {
    int n;
    long long int x, y;
    FILE *fp = fopen("window.in", "r");
    fscanf(fp, "%d\n", &n);
    for(int i = 0; i < n; i++) {
        fscanf(fp, "%lld %lld\n", &x, &y);
        printf("%lld\n", y * x);
    }
    fclose(fp);
}