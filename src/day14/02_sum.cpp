#include <stdio.h>

int main() {
    long long int c = 0, final = 0;
    long long int temp;
    while (scanf("%lld", &temp) != EOF) {
        final += temp;
        c++;
    }
    printf("%lld %lld", c, final);
}