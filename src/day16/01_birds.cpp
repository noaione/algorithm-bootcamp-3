#include <stdio.h>

int migratoryBirds(int arr[]) {
    int highest_id = 1;
    int highest = arr[0];
    for (int i = 0; i < 5; i++) {
        if (arr[i] == highest)
        {
            if (i + 1 < highest_id) {
                highest_id = i + 1;
            }
        } else if (arr[i] >= highest) {
            highest = arr[i];
            highest_id = i + 1;
        }
    }
    return highest_id;
}

int main() {
    int n;
    scanf("%d", &n);
    int arr[5] = {0, 0, 0, 0, 0};
    for (int i = 0; i < n; i++) {
        int t;
        scanf("%d", &t);
        arr[t-1]++;
    }
    int res = migratoryBirds(arr);
    printf("%d", res);
}