#include <stdio.h>

int main() {
    int cheight[26] = {0};
    char word[50];
    for (int i = 0; i < 26; i++) {
        scanf("%d", &cheight[i]);
    }
    scanf("%s", &word);

    int len = 0;
    int highest = cheight[0];
    for (int i = 0; word[i] != '\0'; i++) {
        if (cheight[word[i] - 'a'] > highest) {
            highest = cheight[word[i] - 'a'];
        }
        len++;
    }
    printf("%d", highest * len);
}