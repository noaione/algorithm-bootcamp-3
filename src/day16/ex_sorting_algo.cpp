#include <stdio.h>
#include <stdlib.h>

void shuffle(int arr[], int n)
{
    int i, t, r;
    for(i=0; i < n; i++) {
        t = arr[i];
        r = rand() % n;
        arr[i] = arr[r];
        arr[r] = t;
    }
}


/*
    SORT ALGO #1
    Bubble sort
*/
void bubbleSort(int arr[], int n) {
    bool swapped;
    do {
        swapped = false;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] > arr[i+1]) {
                int temp = arr[i+1];
                arr[i+1] = arr[i];
                arr[i] = temp;
                swapped = true;
            }
        }
        n--;
    } while (swapped);
}

/*
    SORT ALGO #2
    Insertion sort
*/
void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        int cursor = arr[i], j = i - 1;
        while (j >= 0 && cursor < arr[j]) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = cursor;
    }
}

/*
    SORT ALGO #3
    Selection sort
*/
void selectionSort(int arr[], int n) {
    for (int i = 0; i < n - 1; i++) {
        int minDex = i;
        for (int j = i + 1; j < n; j++) {
            if (arr[j] < arr[minDex]) {
                minDex = j;
            }
        }
        int temp = arr[i];
        arr[i] = arr[minDex];
        arr[minDex] = temp;
    }
}

/*
    SORT ALGO #4
    Quick sort
*/
int partition(int arr[], int left, int right) {
    int dex = left;
    int pivot = arr[right];
    while (left <= right)
    {
        if (arr[left] <= pivot) {
            int temp = arr[left];
            arr[left] = arr[dex];
            arr[dex] = temp;
            ++dex;
        }
        ++left;
    }
    return dex - 1;
}

void quickSort(int arr[], int left, int right) {
    if (left >= right) {
        return;
    }
    int mid = partition(arr, left, right);
    quickSort(arr, left, mid - 1);
    quickSort(arr, mid + 1, right);
}

/*
    SORT ALGO #5
    Merge sort
*/
void mergeArray(int arr[], int l, int m, int r) {
    int temp[r - l + 1], p1 = l, p2 = m + 1, k = 0;
    while (p1 <= m && p2 <= r) {
        if (arr[p1] < arr[p2]) {
            temp[k++] = arr[p1++];
        }
        else {
            temp[k++] = arr[p2++];
        }
    }
    while (p1 <= m) {
        temp[k++] = arr[p1++];
    }
    for (int i = 0; i < k; i++) {
        arr[i + l] = temp[i];
    }
}

void mergeSort(int arr[], int l, int r) {
    if (l != r) {
        int m = (l + r) / 2;
        mergeSort(arr, l, m);
        mergeSort(arr, m + 1, r);
        mergeArray(arr, l, m, r);
    }
}


int main() {
    int n;
    scanf("%d", &n);
    int arr[n];
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    quickSort(arr, 0, n - 1);
    printf("Quick Sort: ");
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\nShuffling: ");
    shuffle(arr, n);
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    selectionSort(arr, n);
    printf("Selection Sort: ");
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\nShuffling: ");
    shuffle(arr, n);
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    insertionSort(arr, n);
    printf("Insertion Sort: ");
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\nShuffling: ");
    shuffle(arr, n);
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    bubbleSort(arr, n);
    printf("Bubble Sort: ");
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\nShuffling: ");
    shuffle(arr, n);
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    mergeSort(arr, 0, n - 1);
    printf("Merge Sort: ");
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
}