#include <stdio.h>

int main()
{
    char let[55][55];
    int n, m;
    scanf("%d %d", &n, &m);
    for (int i = 0; i < n; i++)
    {
        scanf("%s", &let[i]);
    }

    // find bounding boxes.
    int t1 = 0, b1 = 0, t2 = 0, b2 = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (let[i][j] == '*')
            {
                t1 = i;
                i = n;
                break;
            }
        }
    }
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (let[j][i] == '*')
            {
                t2 = i;
                i = m;
                break;
            }
        }
    }

    for (int i = n - 1; i >= 0; i--)
    {
        for (int j = 0; j < m; j++)
        {
            if (let[i][j] == '*')
            {
                b1 = i;
                i = 0;
                break;
            }
        }
    }
    for (int i = m - 1; i >= 0; i--)
    {
        for (int j = 0; j < n; j++)
        {
            if (let[j][i] == '*')
            {
                b2 = i;
                i = 0;
                break;
            }
        }
    }

    for (int i = t1; i <= b1; i++)
    {
        for (int j = t2; j <= b2; j++)
        {
            printf("%c", let[i][j]);
        }
        printf("\n");
    }
}