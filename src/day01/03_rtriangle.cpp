#include <stdio.h>

int main() {
    int a, b, c, res;
    scanf("%d %d %d", &a, &b, &c);
    res = a * b / 2;
    printf("%d", res);
    return 0;
};