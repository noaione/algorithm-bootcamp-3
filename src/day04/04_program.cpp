#include <stdio.h>

int main() {
    int n, b;
    float p;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        float bpm, abpm_l, abpm_m, t;
        scanf("%d %f", &b, &p);
        abpm_l = 60 / (p / (b - 1));
        abpm_m = (60 / (p / (b + 1)));
        bpm = (60 * b) / p;
        printf("%.4f %.4f %.4f\n", abpm_l, bpm, abpm_m);
    }
}