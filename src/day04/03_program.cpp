#include <stdio.h>
#include <string.h>

int main() {
    int t;
    char t2[100000];
    scanf("%d", &t);
    for (int i = 0; i < t; i++) {
        int n = 0;
        scanf("%s", &t2);
        for (int i = 0; i < strlen(t2); i++) {
            n += t2[i] - 48;
        }
        printf("%d\n", n);
    }
}