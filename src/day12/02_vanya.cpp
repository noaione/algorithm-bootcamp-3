#include <stdio.h>

struct Scales {
    long long int weight;
    long long int mass;
};

int main() {
    int flag = 1;
    struct Scales Vanya;
    scanf("%lld %lld", &Vanya.weight, &Vanya.mass);
    while (Vanya.mass != 0) {
        if (Vanya.mass % Vanya.weight <= 1) {
            Vanya.mass /= Vanya.weight;
        } else if (Vanya.mass % Vanya.weight == Vanya.weight - 1) {
            Vanya.mass = (Vanya.mass + 1) / Vanya.weight;
        } else {
            flag = 0;
            break;
        }
    }
    if (flag) {
        printf("YES");
        return 0;
    } else {
        printf("NO");
        return 0;
    }
}