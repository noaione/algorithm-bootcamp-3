#include <stdio.h>

struct Letters {
    char text;
};

int main() {
    int ans = 0;
    int known[26] = {};
    struct Letters Anton;
    while (Anton.text != '}') {
        scanf("%c", &Anton.text);
        if (Anton.text >= 'a' && Anton.text <= 'z') {
            known[Anton.text - 'a']++;
        }
    }
    for (int i = 0; i < 26; i++) {
        if (known[i] > 0) {
            ans++;
        }
    }
    printf("\n%d", ans);
}