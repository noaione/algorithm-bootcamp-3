#include <stdio.h>

int main() {
    int withdraw;
    double bal;
    scanf("%d %lf", &withdraw, &bal);
    if (withdraw % 5 != 0) {
        printf("%.2lf", bal);
        return 0;
    }
    double to_withdraw = withdraw + .50;
    if (to_withdraw >= bal) {
        printf("%.2lf", bal);
        return 0;
    }
    printf("%.2lf", bal - to_withdraw);
}