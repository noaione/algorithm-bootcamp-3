#include <stdio.h>

// shorthand
#define ll long long

ll fpb(ll a, ll b) {
    if (b == 0) {
        return a;
    }
    return fpb(b, a % b);
}

ll kpk(ll a, ll b) {
    return (a * b) / fpb(a, b);
}

int main() {
    ll n;
    scanf("%lld", &n);
    ll res[2] = {1, n};

    for (ll i = 1; i * i <= n; i++) {
        if (n % i == 0 && kpk(i, n / i) == n) {
            res[0] = i;
            res[1] = n / i;
        }
    }
    printf("%lld %lld", res[0], res[1]);
}