#include <stdio.h>

int binarySearch(int numData[], int max, int target) {
    int found = numData[0];
    int min = 0;
    while (min <= max) {
        int mid = (max + min) / 2;
        if (numData[mid] == target) {
            return mid;
        }
        else if (numData[mid] < target) {
            min = mid + 1;
        } else {
            max = mid - 1;
        }
    }
    return -1;
}

int main() {
    int array[10] = {1, 2, 3, 10, 20, 23, 29, 42, 50, 53};
    int pos = binarySearch(array, 10, 1);
    printf("%d [%d]", pos, array[pos]);
}