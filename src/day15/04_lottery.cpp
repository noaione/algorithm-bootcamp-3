#include <stdio.h>

int main() {
    long int n, f;
    scanf("%ld", &n);
    f = n / 100;
    f += (n % 100) / 20;
    f += (n % 20) / 10;
    f += (n % 10) / 5;
    f += (n % 5);
    printf("%ld", f);
}