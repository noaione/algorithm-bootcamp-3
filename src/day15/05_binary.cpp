#include <stdio.h>

// shorthand
#define ll long long

ll binarySearch(ll numData[], ll max, ll target) {
    ll found = -1;
    int min = 0;
    while (min <= max) {
        int mid = (max + min) / 2;
        if (numData[mid] == target) {
            found = mid;
            break;
        }
        else if (numData[mid] < target) {
            min = mid + 1;
        } else {
            max = mid - 1;
        }
    }
    return found;
}


int main() {
    ll n, q, array[10005];
    scanf("%lld %lld", &n, &q);
    for (int i = 0; i < n; i++) {
        ll temp, j = 0, array[10005];
        while (scanf("%lld", &temp) != EOF) {
            array[j] = temp;
            j++;
        }
        ll pos = binarySearch(array, j - 1, q);
        if (pos == -1) {
            printf("-1\n");
        } else {
            printf("%lld\n", array[pos]);
        }
        
    }    
}