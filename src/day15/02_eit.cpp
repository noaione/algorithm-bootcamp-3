#include <stdio.h>

int main() {
    long n, k, c = 0;
    scanf("%ld %ld", &n, &k);
    for (int i = 0; i < n; i++) {
        long t;
        scanf("%ld", &t);
        if (t % k == 0) {
            c++;
        }
    }
    printf("%ld", c);
}